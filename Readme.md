# Mac OS X Installation

## Brew

Install [http://brew.sh](http://brew.sh)

## VirtualBox

    brew search virtualbox
    brew install Caskroom/cask/virtualbox

## Vagrant

    brew search vagrant
    brew install Caskroom/cask/vagrant

## Provision

    vagrant up

## Local Machine

Add the following to local `/etc/hosts`

    192.168.33.10 example.local

